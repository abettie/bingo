
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('title-component', require('./components/site/TitleComponent.vue'));
Vue.component('numbers-component', require('./components/site/NumbersComponent.vue'));

let app = new Vue({
    el: '#app',
    data: {
        last_number: 0,  // 直近の数字
        past_numbers: [] // 過去の数字リスト
    },
    mounted: function () {
        // 最初はAPIからデータ取得
        axios.get('/api/numbers').then(res => {
            if (res.data.length === 0) return;

            // データ更新
            let numbers_ordered_by_id = _.orderBy(res.data, "id", "desc");
            this.last_number = numbers_ordered_by_id.shift().number;
            let numbers_ordered_by_number = _.orderBy(numbers_ordered_by_id, "number", "asc");
            this.past_numbers = _.map(numbers_ordered_by_number, d => {
                return d.number;
            });
        });
        // ツールによる追加をリスニング
        Echo.channel("bingo").listen("MessageReceived", e => {
            if (e.numbers.length === 0) {
                this.last_number = 0;
                this.past_numbers = [];
            }

            // データ更新
            let numbers_ordered_by_id = _.orderBy(e.numbers, "id", "desc");
            this.last_number = numbers_ordered_by_id.shift().number;
            let numbers_ordered_by_number = _.orderBy(numbers_ordered_by_id, "number", "asc");
            this.past_numbers = _.map(numbers_ordered_by_number, d => {
                return d.number;
            });
        });
    },
});

