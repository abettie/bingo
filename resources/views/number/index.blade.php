@extends('layouts.app')

@section('content')
    <div id="app">
        <title-component></title-component>
        <numbers-component
            :last_number="last_number"
            :past_numbers="past_numbers"
        ></numbers-component>
    </div>
@endsection
