@extends('layouts.tool')

@section('content')
    <div id="app">
        <input-number-component></input-number-component>
        <tool-numbers-component
            v-bind:past_numbers="past_numbers"
            v-bind:last_number="last_number"></tool-numbers-component>
    </div>
@endsection
