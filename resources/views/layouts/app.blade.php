<!doctype html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>BINGO NUMBERS</title>
    <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
    <script src="https://ws-bingo.makedara.work/socket.io/socket.io.js"></script>
</head>
<body>
    <div class="container">
        @yield('content')
    </div>
</body>
<script src="{{ mix('/js/app.js') }}"></script>
</html>
