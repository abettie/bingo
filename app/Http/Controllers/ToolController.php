<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ToolController extends Controller
{
    /**
     * ツールトップページ
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("number.tool");
    }
}
