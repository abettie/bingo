<?php

namespace App\Http\Controllers\Api;

use App\Events\MessageReceived;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    /**
     * メッセージが来たぞ
     *
     * @param \Illuminate\Http\Request  $request
     */
    public function store(Request $request) {
        $message = $request->input("message", null);

        event(new MessageReceived($message));

        return response()->json($message);
    }
}
